// import built-in react modules
import ReactDOM   from 'react-dom/client';
import React      from 'react';

// import css file
import App        from './App';

// import bootstrap css
import "bootswatch/dist/flatly/bootstrap.min.css";

// render the app
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

