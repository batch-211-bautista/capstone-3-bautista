// import built-in react modules
import { Navigate } 	from 'react-router-dom';

// import components
import Banner 			from '../components/Banner';

// import gif from assets folder
import Forbidden 		from '../assets/403 Error Forbidden.gif';

// export the function so that it can be use anywhere
export default function ForbiddenPage() {

	/* declare an object to hold the details 
	to be passed as props to the banner component */
	const data = {
		source: `${Forbidden}`,
		alt: "403_forbidden",
		submessage: "You don't have permission to access this resource.",
		label: "Return",
		route: "/products"
	}

	// render the component
	return (
		// if user is authenticated, this is accessible
		(localStorage.getItem('token') !== null) ?
			<>
				<Banner bannerProp={data} />
			</>
		:
		// else, redirect to 404 not found page
			<Navigate to="*"/>
	)
}