// import built-in react modules
import { Navigate, useNavigate } 		from 'react-router-dom';
import { useContext } 					from 'react';
import React 							from 'react';
// import UserContext
import UserContext 						from '../UserContext';

// import component from react bootstrap
import { Container } 					from 'react-bootstrap';
// import Swal
import Swal 							from 'sweetalert2';
// import formik
import { useFormik } 					from 'formik';

// import primereact dependencies
import { InputText } 					from 'primereact/inputtext';
import { Button } 						from 'primereact/button';
import { Password } 					from 'primereact/password';
import { Checkbox } 					from 'primereact/checkbox';
import { Divider } 						from 'primereact/divider';
import { classNames }					from 'primereact/utils';

// import primeicons, primereact and primeflex dependencies
import 'primereact/resources/themes/lara-light-teal/theme.css';
import 'primereact/resources/primereact.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

// import component
import Footer 							from '../components/Footer';
// import image from assets
import RegisterImg 						from '../assets/intro-register.jpg';


// export the function so that it can be use anywhere
export default function Register() {

	// set user from UserContext
	const { user } = useContext(UserContext);

	// set navigate to redirect to another page
	const navigate = useNavigate();

    // set the initial values on the fields and declare validations
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            mobileNo: '',
            password1: '',
            password2: '',
            accept: false
        },
        validate: (data) => {
            let errors = {};

            if (!data.firstName) {
                errors.firstName = 'First name is required.';
            }

            if (!data.lastName) {
                errors.lastName = 'Last name is required.';
            }

            if (!data.mobileNo) {
                errors.mobileNo = 'Mobile number is required.';
            }

            if (!data.email) {
                errors.email = 'Email is required.';
            }
            else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(data.email)) {
                errors.email = 'Invalid email address. E.g. example@email.com';
            }

            if (!data.password1) {
                errors.password1 = 'Password is required.';
            }

            if (!data.password2) {
                errors.password2 = 'Verify your password';
            }

            if (data.password1 !== data.password2) {
            	errors.password2 = 'Password does not match';
            }

            if (!data.accept) {
                errors.accept = 'You need to agree to the terms and conditions.';
            }

            return errors;
        },
        // this serves as the submit function on formik
        onSubmit: (data) => {
            // invoke the registerUser function
            registerUser();
            // clears the form upon submit
            formik.resetForm();
        }
    });

    // function to register the user based from the formik values
    function registerUser() {
    	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				firstName: formik.values.firstName,
				lastName: formik.values.lastName,
				email: formik.values.email,
				password: formik.values.password1,
				mobileNo: formik.values.mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message === "Successfully Registered!") {
	            Swal.fire({
					position: 'center',
					icon: 'success',
					title: 'Registration successful!',
					text: `Your account is registered under name ${formik.values.firstName} ${formik.values.lastName}.`,
					showConfirmButton: false,
					timer: 2000
				})
				navigate("/login")
			} else {
				Swal.fire({
	                title: "Registration failed!",
	                icon: "error",
	                text: "Email address is already taken!"
	            })
			}
		});
    }

    // set variables that will hold the error messages for validation
    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    // this suggestion dialog will appear once the user types in a password on the password field
    const passwordHeader = <h6>Pick a password</h6>;
    const passwordFooter = (
        <React.Fragment>
            <Divider />
            <p className="mt-2">Suggestions</p>
            <ul className="pl-2 ml-2 mt-0" style={{ lineHeight: '1.5' }}>
                <li>At least one lowercase</li>
                <li>At least one uppercase</li>
                <li>At least one numeric</li>
                <li>Minimum 8 characters</li>
            </ul>
        </React.Fragment>
    );    

    // render the components
	return (
		// if the user is authenticated, redirect to products page
		(user.id !== null) ?
			<Navigate to="/products" />
		:
		<>
			{/* Intro image */}
			<div className="pt-5 pt-md-8">
				<img src={RegisterImg} alt="register_picture" className="register-picture" />
				<div className="overlay"></div>
				<div className="intro-text content-register">
		        	<div className="hero">
		        		<h2 className="title shadow-text">Sign up now<br/>to get discount vouchers</h2>
		        		<h2 className="sub-title">Get a chance to avail exclusive promos</h2>
		        		<p className="description">Get access to bike parts and components from different known brands. Check out the latest Shimano Dura Ace Di2 groupset for your road bike or maybe just buy a new bike?</p>
		        	</div>
		    	</div>
			</div>

			<Container fluid className="px-0">
				{/* Register form from primereact */}
				<div className="form-demo">
		            <div className="d-flex flex-column align-items-center align-items-lg-end register-login-card mx-3 my-5">
		                <div className="card bg-light rounded-0 borderless shadow-box p-4">
		                    <h3 className="text-center">Sign up</h3>
		                    <form onSubmit={formik.handleSubmit} className="p-fluid p-3">
		                        <div className="field">
		                            <span className="p-float-label">
		                                <InputText id="firstName" name="firstName" value={formik.values.firstName} onChange={formik.handleChange} 
		                                	className={classNames({ 'p-invalid': isFormFieldValid('firstName') })} />
		                                <label htmlFor="firstName" className={classNames({ 'p-error': isFormFieldValid('firstName') })}>First name*</label>
		                            </span>
		                            {getFormErrorMessage('firstName')}
		                        </div>
		                        <div className="field">
		                            <span className="p-float-label">
		                                <InputText id="lastName" name="lastName" value={formik.values.lastName} onChange={formik.handleChange} 
		                                	className={classNames({ 'p-invalid': isFormFieldValid('lastName') })} />
		                                <label htmlFor="lastName" className={classNames({ 'p-error': isFormFieldValid('lastName') })}>Last name*</label>
		                            </span>
		                            {getFormErrorMessage('lastName')}
		                        </div>
		                        <div className="field">
		                            <span className="p-float-label p-input-icon-right">
		                                <i className="pi pi-mobile" />
		                                <InputText id="mobileNo" name="mobileNo" value={formik.values.mobileNo} onChange={formik.handleChange} 
		                                	className={classNames({ 'p-invalid': isFormFieldValid('mobileNo') })} />
		                                <label htmlFor="mobileNo" className={classNames({ 'p-error': isFormFieldValid('mobileNo') })}>Mobile No*</label>
		                            </span>
		                            {getFormErrorMessage('mobileNo')}
		                        </div>
		                        <div className="field">
		                            <span className="p-float-label p-input-icon-right">
		                                <i className="pi pi-envelope" />
		                                <InputText id="email" name="email" value={formik.values.email} onChange={formik.handleChange} 
		                                	className={classNames({ 'p-invalid': isFormFieldValid('email') })} />
		                                <label htmlFor="email" className={classNames({ 'p-error': isFormFieldValid('email') })}>Email*</label>
		                            </span>
		                            {getFormErrorMessage('email')}
		                        </div>
		                        <div className="field">
		                            <span className="p-float-label">
		                                <Password id="password1" name="password1" value={formik.values.password1} onChange={formik.handleChange} toggleMask
		                                    className={classNames({ 'p-invalid': isFormFieldValid('password') })} header={passwordHeader} footer={passwordFooter} />
		                                <label htmlFor="password" className={classNames({ 'p-error': isFormFieldValid('password1') })}>Password*</label>
		                            </span>
		                            {getFormErrorMessage('password1')}
		                        </div>
		                        <div className="field">
		                            <span className="p-float-label">
		                                <Password id="password2" name="password2" value={formik.values.password2} onChange={formik.handleChange} toggleMask 
		                                	className={classNames({ 'p-invalid': isFormFieldValid('password') })} />
		                                <label htmlFor="password" className={classNames({ 'p-error': isFormFieldValid('password2') })}>Verify password*</label>
		                            </span>
		                            {getFormErrorMessage('password2')}
		                        </div>
		                        <div className="field-checkbox">
		                            <Checkbox inputId="accept" name="accept" checked={formik.values.accept} onChange={formik.handleChange} 
		                            	className={classNames({ 'p-invalid': isFormFieldValid('accept') })} />
		                            <label htmlFor="accept" className={classNames({ 'p-error': isFormFieldValid('accept') })}>I agree to the terms and conditions*</label>
		                        </div>
		                        <Button type="submit" label="Submit" className="mt-2" />
		                    </form>
		                </div>
		            </div>
		        </div>
			</Container>
			<Footer/>
		</>
	)
}