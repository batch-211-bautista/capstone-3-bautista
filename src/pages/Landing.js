// import components
import VideoBackground 		from '../components/VideoBackground';
import FeaturedProducts		from '../components/FeaturedProducts';
import ExclusiveProduct 	from '../components/ExclusiveProduct';
import FeaturedBrands 		from '../components/FeaturedBrands';
import LandingPageFooter 	from '../components/LandingPageFooter';


// render the components
export default function Landing() {
	return (
		<div className="pt-5">
	   		<VideoBackground />
	      	<FeaturedProducts />
	      	<ExclusiveProduct />
	      	<FeaturedBrands />
	      	<LandingPageFooter />
		</div>
	)
}