// import built-in react modules
import { Navigate } 	from 'react-router-dom';

// import components
import Banner 			from '../components/Banner';

// import gif from assets folder
import Unauthorized 	from '../assets/401 Error Unauthorized.gif';

// export the function so that it can be use anywhere
export default function UnauthorizedPage() {

	// declare an object to hold the details to be passed as props to the banner component
	const data = {
		source: `${Unauthorized}`,
		alt: "401_unauthorized",
		submessage: "Authorization required",
		label: "Return",
		route: "/"
	}

	// render the components
	return (
		// if user is authenticated, this is accessible
		(localStorage.getItem('token') !== null) ?
			<Banner bannerProp={data} />
		:
		// else, redirect to 404 not found page
			<Navigate to="*"/>
	)
}