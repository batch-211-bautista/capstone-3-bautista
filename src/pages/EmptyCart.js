// import built-in react modules
import { Navigate } 		from 'react-router-dom';
import { useContext } 		from 'react';

// import UserContext
import UserContext 			from '../UserContext';

// import components
import Banner 				from '../components/Banner';

// import gif from assets folder
import Cart			from '../assets/Add to Cart.gif';

// export the function so that it can be use anywhere
export default function EmptyCart() {

	// get the user from UserContext
	const { user } = useContext(UserContext);

	/* declare an object to hold the details 
	to be passed as props to the banner component */
	const data = {
		message: "Your cart is empty",
		source: `${Cart}`,
		alt: "empty_cart",
		label: "Continue shopping",
		route: "/products"
	}

	// render the component
	return (
		// if user is authenticated, this is accessible
		(user.id !== null) ?
			<Banner bannerProp={data} />
		:
		// else, redirect to 404 not found page
			<Navigate to="*" />
	)
}