// import built-in react components
import { Navigate } 		from 'react-router-dom';
import { useContext } 		from 'react';
// import UserContext
import UserContext 			from '../UserContext';

// import components
import Banner 				from '../components/Banner';

// import gif from assets folder
import Order   				from '../assets/Free shipping.gif';

// export the function so that it can be use anywhere
export default function EmptyOrder() {

	// get the user from UserContext
	const { user } = useContext(UserContext);

	/* declare an object to hold the details 
	to be passed as props to the banner component */
	const data = {
		message: "No orders yet",
		source: `${Order}`,
		alt: "empty_order",
		label: "Continue shopping",
		route: "/products"
	}

	// render the component
	return (
		// if user is authenticated, this is accessible
		(user.id !== null) ?
			<Banner bannerProp={data} />
		:
		// else, redirect to 404 not found page
			<Navigate to="*" />
	)
}