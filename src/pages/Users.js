// import built-in react modules
import { 
	useEffect,
	useState,
	useContext 
} 							    from 'react';
import { useNavigate } 			from 'react-router-dom';
// import UserContext
import UserContext          	from '../UserContext';

// import elements from react bootstrap
import { Container, Row, Col }	from 'react-bootstrap';

// import components
import PaginationComp       	from '../components/PaginationComp';
import UserCard 	        	from '../components/UserCard';
import Footer 		        	from '../components/Footer';


// export the function so that it can be use anywhere
export default function Users() {

	// set state to store an array of user details
	const [userData, setUserData] = useState([]);

	// import user from UserContext
	const { user } = useContext(UserContext);

	// set navigate variable to redirect to another page
	const navigate = useNavigate();

	/*
		set states used for pagination
		1. currentPage - contains the current page
		2. postPerPage - contains how many post are allowed per page
		3. totalUsers - holds the length of data from fetch
	*/
	const [currentPage, setCurrentPage] = useState(1);
	const [postPerPage, setPostPerPage] = useState('');
	const [totalUsers, setTotalUsers] = useState('');

	// this is a function to fetch all the users and their details from the database
	function fetchAllUsers() {
		// condition to check if the user is authenticated and an admin
		if (user.id !== null && user.isAdmin === true) {
			fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// sets totalUsers based from the length of data.details
				setTotalUsers(data.details.length);
				// set the post per page as 4
				setPostPerPage(4);

				// logic to get the firstPostIndex and lastPostIndex based from the currentPage and postPerPage
				const lastPostIndex = currentPage * postPerPage;
				const firstPostIndex = lastPostIndex - postPerPage;

				// slice the array to get the only the data based on the first and last index of the array
				const currentPosts = data.details.slice(firstPostIndex, lastPostIndex);

				// use map to render the UserCard and return a new array assigned to userData
				let userData = (currentPosts.map(users => {
					return (
						<Col key={users._id} md="auto">
							<UserCard userProp={users} key={users._id}/>
						</Col>
					)
				}));

				// set the userData state
				setUserData(userData);
			});

		// if the user is not an admin, redirect to forbidden page
		} else if (user.isAdmin === false) {
			navigate("/forbidden");

		// if the user is unauthenticated, redirect to unauthorized page
		} else if (localStorage.getItem('token') == null) {
			navigate("/unauthorized");
		}
	}

	// pass the fetchAllUsers function to run it at first page render
	useEffect(() => {
		fetchAllUsers();
	});
	// [] dependencies are optional

	// render the components
	return (
		<>
			{/* Users body section */}
			<Container className="my-7 py-4 px-3">
				<Row>
					{userData}
				</Row>

				{/* Pagination */}
				<PaginationComp totalPosts={totalUsers} postsPerPage={postPerPage} setCurrentPage={setCurrentPage} currentPage={currentPage} />
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>
	)
}