// import built-in react modules
import { useEffect, useState, useContext } 		from 'react';
import { useNavigate, Link } 					from 'react-router-dom';

// import UserContext
import UserContext 								from '../UserContext';

// import components from react bootstrap
import { Container, Button } 					from 'react-bootstrap';

// import components
import PaginationComp 							from '../components/PaginationComp';
import OrderCard 								from '../components/OrderCard';
import Footer 									from '../components/Footer';

// import image from assets folder
import OrderHistoryImg 							from '../assets/intro-order-history.jpg';


// export the function so that it can be use anywhere
export default function Orders() {

	// set state array for the orders
	const [orders, setOrders] = useState([]);

	// get user from UserContext
	const { user } = useContext(UserContext);

	// set navigate for navigating to other pages
	const navigate = useNavigate();

	/* set states used for pagination
		1. currentPage - contains the current page
		2. postPerPage - contains how many post are allowed per page
		3. totalPosts - holds the length of data from fetch */
	const [currentPage, setCurrentPage] = useState(1);
	const [postPerPage, setPostPerPage] = useState('');
	const [totalOrders, setTotalOrders] = useState('');

	// function for fetching the orders of the authenticated user
	function fetchOrderData() {
		// condition to check if the user is authenticated and not an admin
		if (user.id !== null && user.isAdmin === false) {
			fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// sets totalOrders based from the length of data.orders
				setTotalOrders(data.orders.length);
				// set the post per page as 4
				setPostPerPage(4);

				// logic to get the firstPostIndex and lastPostIndex based from the currentPage and postPerPage
				const lastPostIndex = currentPage * postPerPage;
				const firstPostIndex = lastPostIndex - postPerPage;

				// slice the array to get the only the data based on the first and last index of the array
				const currentPosts = data.orders.slice(firstPostIndex, lastPostIndex);

				// condition to check if the data.orders array is not empty
				if (data.orders.length !== 0) {
					// use map to render the OrderCard and return a new array assigned to orders
					let orders = currentPosts.map(orders => {
						return <OrderCard orderProp={orders} key={orders.id}/>
					})

					// set the orders state array
					setOrders(orders);

				// if data.orders array is empty, redirect to emptyOrder page
				} else {
					navigate("/emptyOrder");
				}
				
			});

		// if the user is an admin, redirect to forbidden page
		} else if (user.isAdmin === true) {
			navigate("/forbidden");

		// if the user is not authenticated, redirect to unauthorized
		} else if (localStorage.getItem('token') == null) {
			navigate("/unauthorized");
		}
	}

	// pass the fetchOrderData on useEffect to run it at first page render
	useEffect(() => {
		fetchOrderData();
	});
	// dependencies are optional

	// render the components
	return (
		<>
			{/* Intro Image */}
			<div className="min-vh-100">
				<img src={OrderHistoryImg} alt="intro_picture" className="intro-picture"/>
				<div className="overlay"></div>
				<div className="intro-text-profile content">				
		        	<div className="hero">		    
		        		<h2 className="title shadow-text">Order history</h2>
		        	</div>
		    	</div>
			</div>

			{/* Order history body section */}
			<Container className="my-4 py-4">
				<div className="d-flex justify-content-center justify-content-lg-end">
					<Button as={Link} to="/products" className="cta primary-background mb-3 py-2 pe-1">
					    <span className="hover-underline-animation">Continue shopping</span>
					    <i className='bx bx-right-arrow-alt continue-arrow'></i>
					</Button>
				</div>
				{orders}

				{/* Pagination */}
				<PaginationComp totalPosts={totalOrders} postsPerPage={postPerPage} setCurrentPage={setCurrentPage} currentPage={currentPage} />
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>
	)
}