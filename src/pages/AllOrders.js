// import built-in react modules
import { useEffect, useState, useContext } 			from 'react';
import { useNavigate } 								from 'react-router-dom';
// import UserContext
import UserContext 									from '../UserContext';

// import component from react bootstrap
import { Container } 								from 'react-bootstrap';

// import components
import AllUsersOrderCard 							from '../components/AllUsersOrderCard';
import PaginationComp 								from '../components/PaginationComp';
import Footer 										from '../components/Footer';


// export the function so that it can be use anywhere
export default function AllOrders() {

	// set order state array to hold All users order data
	const [order, setOrder] = useState([]);

	// get user from UserContext
	const { user } = useContext(UserContext);

	// set navigate for navigating to other pages
	const navigate = useNavigate();

	/* set states used for pagination
		1. currentPage - contains the current page
		2. postPerPage - contains how many post are allowed per page
		3. totalPosts - holds the length of data from fetch */
	const [currentPage, setCurrentPage] = useState(1);
	const [postPerPage, setPostPerPage] = useState('');
	const [totalOrders, setTotalOrders] = useState('');

	// function for fetching all user's order
	function fetchAllOrders() {
		// condition to check if user is authenticated and is an admin
		if (user.id !== null && user.isAdmin === true) {
			fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// sets totalOrders based from the length of data.orders
				setTotalOrders(data.orders.length);
				// set the post per page as 4
				setPostPerPage(4)

				// logic to get the firstPostIndex and lastPostIndex based from the currentPage and postPerPage
				const lastPostIndex = currentPage * postPerPage;
				const firstPostIndex = lastPostIndex - postPerPage;

				// slice the array to get the only the data based on the first and last index of the array
				const currentPosts = data.orders.slice(firstPostIndex, lastPostIndex);

				// condition to check if the data.orders array is not empty
				if (data.orders.length !== 0) {
					// use map to render the AllUsersOrderCard and return a new array assigned to orderData
					let orderData = currentPosts.map((orders, index) => {
						return (
							<AllUsersOrderCard orderProp={orders} key={orders.id}/>
						)
					});

					// set the order state array
					setOrder(orderData);

				// if data.orders array is empty, redirect to noOrders page
				} else {
					navigate("/noOrders");
				}
			});

		// if the user is an admin, redirect to forbidden page
		} else if (user.isAdmin === false) {
			navigate("/forbidden");

		// if the user is not authenticated, redirect to unauthorized
		} else if (localStorage.getItem('token') == null) {
			navigate("/unauthorized");
		}
	}

	// pass the fetchAllOrders to useEffect to run it at first page render
	useEffect(() => {
		fetchAllOrders();
	});
	// dependencies are optional

	// render the components
	return (
		<>
			{/* All user's order body section */}
			<Container className="my-5 pb-4 pt-8">
				{order}

				{/* Pagination */}
				<PaginationComp totalPosts={totalOrders} postsPerPage={postPerPage} setCurrentPage={setCurrentPage} currentPage={currentPage} />
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>
	)
}