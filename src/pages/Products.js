// import built-in react modules
import { useEffect, useState, useContext } 					from 'react';
import { useNavigate } 										from 'react-router-dom';
// import UserContext
import UserContext 											from '../UserContext';

// import components from react bootstrap
import { Container, Button, Badge, Modal, Form, Row, Col}	from 'react-bootstrap';
// import Swal
import Swal 												from 'sweetalert2';

// import components
import PendingItemCard 										from '../components/PendingItemCard';
import PaginationComp 										from '../components/PaginationComp';
import ProductCard 											from '../components/ProductCard';
import Footer 												from '../components/Footer';

// import image from assets folder
import Intro 												from '../assets/man-riding-mountain-bike.jpg';


// export the function so that it can be use anywhere
export default function Products() {
	
	// set navigate for redirecting to other page
	const navigate = useNavigate();

	// set state array that will hold the products data
	const [products, setProducts] = useState([]);

	// get everything from the userContext
	const { 
		user,
		orderDetails,
		setOrderDetails,
		addToCartState,
		setAddToCartState,
		buyNowState,
		setBuyNowState 
	} = useContext(UserContext);

	// set states that will hold product data for add product
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [source, setSource] = useState('');

	// set boolean state that will enable save button on conditional rendering
	const [isComplete, setIsComplete] = useState(false);

	// set state array that will hold the product data on queue
	const [pendingItemData, setPendingItemData] = useState([]);

	/* set states used for pagination
		1. currentPage - contains the current page
		2. postPerPage - contains how many post are allowed per page
		3. totalPosts - holds the length of data from fetch */
	const [currentPage, setCurrentPage] = useState(1);
	const [postPerPage, setPostPerPage] = useState('');
	const [totalPosts, setTotalPosts] = useState('');

	// set state for the add product modal
	const [show, setShow] = useState(false);
	// set close function for add product modal
	const handleClose = () => {
		setShow(false);
		setName('');
		setDescription('');
		setPrice('');
		setSource('');
			
	}
	// set show function for add product modal
  	const handleShow = () => setShow(true);

  	// set state and close-show function for offCanvas
  	const [showOffCanvas, setShowOffCanvas] = useState(false);
    const handleCloseOffCanvas = () => setShowOffCanvas(false);
    const handleShowOffCanvas = () => setShowOffCanvas(true);

    // function to fetch active products
  	function fetchActiveProduct() {
  		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			// sets totalPosts based from the length of data.products
			setTotalPosts(data.products.length);
			// set the post per page as 9
			setPostPerPage(9);

			// logic to get the firstPostIndex and lastPostIndex based from the currentPage and postPerPage
			const lastPostIndex = currentPage * postPerPage;
			const firstPostIndex = lastPostIndex - postPerPage;

			// slice the array to get the only the data based on the first and last index of the array
			const currentPosts = data.products.slice(firstPostIndex, lastPostIndex);

			// use map to render the ProductCard and return a new array assigned to productArr
			const productArr = currentPosts.map(product => {
				return (
					<Col key={product.name.toString()} xs="auto">
						<ProductCard productProp={product} key={product._id.toString()} />
					</Col>
				)
			});

			// set the products state array
			setProducts(productArr);
		});
  	}

  	// function to fetch all products
  	function fetchAllProduct() {
  		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// sets totalPosts based from the length of data.products
			setTotalPosts(data.products.length);
			// set the post per page as 9
			setPostPerPage(9);

			// logic to get the firstPostIndex and lastPostIndex based from the currentPage and postPerPage
			const lastPostIndex = currentPage * postPerPage;
			const firstPostIndex = lastPostIndex - postPerPage;

			// slice the array to get the only the data based on the first and last index of the array
			const currentPosts = data.products.slice(firstPostIndex, lastPostIndex);

			// use map to render the ProductCard and return a new array assigned to productArr
			const productArr = currentPosts.map(product => {
				return (
					<Col key={product.name.toString()} xs="auto">
						<ProductCard productProp={product} key={product._id.toString()} />
					</Col>
				)
			});

			// set the products state array
			setProducts(productArr);
		});
  	}

  	// function to fetch the details of product on orderDetails
  	function fetchPendingItemData(orderDetails) {
  		// use slice to get the last product from the array
  		const lastDetail = orderDetails.slice(-1)

  		// loop through the lastDetail to get the productId
  		for (let detail of lastDetail) {
  			fetch(`${process.env.REACT_APP_API_URL}/products/${detail.productId}`)
	  		.then(res => res.json())
	  		.then(data => {
	  			// render the PendingItemCard component
	  			return setPendingItemData(
	  				<PendingItemCard pendingItemProp={data} pendingQuantityProp={detail.quantity} key={data.product._id} />
	  			)
	  		})
  		}
  	}

  	/* pass the fetchActiveProduct and fetchAllProduct 
  	on useEffect to run it at first page render */
	useEffect(() => {
		/*condition to check if the user is not authenticated or is not an admin,
		then run the fetchActiveProduct function*/
		if (user.id === null || user.isAdmin === false) {
			fetchActiveProduct();

		// else run the fetchAllProduct function
		} else {
			fetchAllProduct();
		}
	});

	/* this useEffect will check if the name, description, price 
	and source is not empty on the add product modal */
	useEffect(() => {
		// if not empty, setIsComplete will set to true, otherwise set to false
		if (name !== "" && description !== "" && price !== "" && source !== "") {
			setIsComplete(true);
		} else {
			setIsComplete(false);
		}
	},[name, description, price, source]);

	// pass the fetchPendingItemData function to run it at first page render
	useEffect(() => {
		fetchPendingItemData(orderDetails);
	},[orderDetails]);

	// function on adding a new product, for admin only
	function addProduct(e) {
		// use preventDefault to prevent the page from refreshing/reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify([
				{
					name: name,
					description: description,
					price: price,
					source: source
				}
			])
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert once operation is successful
			if (data.message === "Product added successfully!") {
				/*handleClose will close the add new product modal 
				so that the Swal alert can show up*/
				handleClose();
				Swal.fire({
	               title: "Product added successfully!",
	               icon: "success"
	           	})
	        // if product already exist, it will show an alert as well
			} else if (data.message === "Operation failed: product already exists") {
				/*handleClose will close the add new product modal 
				so that the Swal alert can show up*/
				handleClose();
				Swal.fire({
	               title: "Operation failed",
	               icon: "error",
	               text: "Product already exists!"
            	})
			}

			// clears the name, description, price and source field
			setName('');
			setDescription('');
			setPrice('');
			setSource('');
		});
	}

	// function on adding a product to your cart
	function addToCart(e, orderDetails) {
		// use preventDefault to prevent the page from refreshing/reloading
  		e.preventDefault();

  		fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`, {
  			method: 'POST',
  			headers: {
  				'Content-Type': 'application/json',
  				Authorization: `Bearer ${localStorage.getItem('token')}`
  			},
  			body: JSON.stringify(orderDetails)
  		})
  		.then(res => res.json())
  		.then(data => {
  			/* handleCloseOffCanvas will close the check queue modal */
  			handleCloseOffCanvas();

  			// shows an alert if operation is successful
  			if (data.message === "Items added to cart") {
  				Swal.fire({
		         	title: "Item added to cart!",
		         	icon: "success"
        		})
        		// resets the orderDetails to an empty array
  				setOrderDetails([]);
  				// navigate to cart page after successful operation
            	navigate("/cart");

            // shows an alert if operation failed
  			} else {
				Swal.fire({
	            	title: "Operation failed",
	            	icon: "error",
	         	})
	         	// resets the orderDetails to an empty array
  				setOrderDetails([]);
  			}
  			// set the setAddToCartState to false again
  			setAddToCartState(false);
  		});
  	}

  	// function for creating an order
  	function createOrder(e, orderDetails) {
  		// use preventDefault to prevent the page from refreshing/reloading
  		e.preventDefault();

  		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
  			method: 'POST',
  			headers: {
  				'Content-Type': 'application/json',
  				Authorization: `Bearer ${localStorage.getItem('token')}`
  			},
  			body: JSON.stringify(orderDetails)
  		})
  		.then(res => res.json())
  		.then(data => {
  			/* handleCloseOffCanvas will close the check queue modal */
  			handleCloseOffCanvas();

  			// shows an alert if operation is successful
  			if (data.message === "Order created") {
  				Swal.fire({
		         	title: "Order created successfully!",
		         	icon: "success"
	         	})
	         	// resets the orderDetails to an empty array
	  			setOrderDetails([]);
	  			// navigate to cart page after successful operation
	            navigate("/orders");

	        // shows an alert if operation failed
  			} else {
  				Swal.fire({
		            title: "Operation failed",
		            icon: "error",
	        	})
	  			// resets the orderDetails to an empty array
		        setOrderDetails([]);
  			}
  			// set the setBuyNowState to false again
  			setBuyNowState(false);
  		});
  	}

  	// render the components
	return (
		<>
			{/* Modal for add new product */}
			<Modal show={show} onHide={handleClose} centered>
				<Modal.Header className="primary-background text-white" closeButton>
					<Modal.Title>New product</Modal.Title>
				</Modal.Header>
				<Modal.Body className="primary-background text-white">
					<Form onSubmit={(e) => addProduct(e)}>
						<Form.Group className="mb-3" controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								type="text"
								placeholder="Enter name"
								value={name}
								onChange={e => setName(e.target.value)} 
								autoFocus
							/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="productDescription" required>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								as="textarea" 
								rows={3} 
								value={description}
								placeholder="Enter description"
								onChange={e => setDescription(e.target.value)}
							/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								placeholder="Enter a number"
								min={0}
								max={500000}
								step={100}
								value={price}
								onChange={e => setPrice(e.target.value)} 
							/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="productSource">
							<Form.Label className="mb-0">Image source</Form.Label>
							<Form.Text className="text-muted d-inline-block my-2">
          						The recommended image resolution is 800 x 800 pixels.
        					</Form.Text>
							<Form.Control
								type="text"
								placeholder="Enter url"
								value={source}
								onChange={e => setSource(e.target.value)}					
							/>
						</Form.Group>
						<div className="d-flex justify-content-end">
							{ (isComplete) ? 
								<Button variant="info" className="m-1" type="submit">Save</Button>
								:
								<Button variant="info" className="m-1" disabled>Save</Button>
							}
							<Button variant="danger" className="m-1" onClick={handleClose}>
								Cancel
							</Button>
						</div>
					</Form>
				</Modal.Body>
			</Modal>

			{/* Modal for check queue */}
			<Modal show={showOffCanvas} onHide={handleCloseOffCanvas} centered className="text-white">
				<Modal.Header className="primary-background" closeButton>
					<Modal.Title>Previous item added on queue</Modal.Title>
				</Modal.Header>
				<Modal.Body className="primary-background">
					<div className="primary-background text-white rounded px-4">
						<div className="d-flex justify-content-start">
							<div className="bg-light text-primary rounded p-2">
					      	Items on queue <Badge bg="danger">{orderDetails.length}</Badge>
					    	</div>
				    	</div>
						<div className="d-flex flex-column justify-content-between">
							{pendingItemData}
						</div>
					</div>
					<div className="d-flex justify-content-end gap-2 mx-4">
						{
							(addToCartState) ?
								<Button variant="primary" className="offwhite" onClick={e => addToCart(e, orderDetails)}>
									<i className='bx bx-cart-alt'></i> 
									Add to cart
								</Button>
							:
								<Button variant="primary" className="offwhite" onClick={e => createOrder(e, orderDetails)}>
									<i className='bx bx-check-square'></i> 
									Checkout
								</Button>
						}
					</div>
				</Modal.Body>
			</Modal>

			{/* Intro image will only show for authenticated or not authenticated normal user */}
			{ ((user.isAdmin) === false || (user.id) === null) && 
			<div className="min-vh-100">
				<img src={Intro} alt="intro_picture" className="intro-picture"/>
				<div className="overlay"></div>
				<div className="intro-text content">
		        	<div className="hero">
		        		<h2 className="title shadow-text">Need to upgrade your bike?<br/>You came to the right place</h2>
		        		<h2 className="sub-title">Add to cart now while supplies last</h2>
		        		<p className="description">Get access to bike parts and components from different known brands. Check out the latest Shimano Dura Ace Di2 groupset for your road bike or maybe just buy a new bike?</p>
		        	</div>
		    	</div>
			</div> }

			{/* Products catalog section */}
			<Container className="my-5 py-5">
				<div className="d-flex justify-content-center justify-content-lg-end">
					{/* Add product button will only show for admin */}
					{ (user.isAdmin === true) ?
						<Button className="btn btn-primary my-3" onClick={handleShow}><i className='bx bx-plus'></i> Add product</Button>
					:
						(addToCartState === true || buyNowState === true) && <Button className="btn btn-primary my-3" onClick={handleShowOffCanvas}>Check queue</Button>
					}
				</div>
				<Row>
					{products}
				</Row>
				<PaginationComp totalPosts={totalPosts} postsPerPage={postPerPage} setCurrentPage={setCurrentPage} currentPage={currentPage} />
			</Container>

			{/* Fixed bottom footer */}
			<Footer />
		</>
	)
}