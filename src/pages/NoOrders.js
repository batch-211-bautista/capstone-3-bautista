// import built-in react modules
import { Navigate } 		from 'react-router-dom';
import { useContext } 		from 'react';
import UserContext 			from '../UserContext';

// import components
import Banner 				from '../components/Banner';

// import gif from assets folder
import NoOrder 				from '../assets/Ecommerce checkout laptop.gif';


// export the function so that it can be use anywhere
export default function NoOrders() {

	// get user from UserContext
	const { user } = useContext(UserContext);

	/* declare an object to hold the details 
	to be passed as props to the banner component */
	const data = {
		message: "No orders yet",
		source: `${NoOrder}`,
		alt: "no_orders_yet",
		label: "Return",
		route: "/products"
	}

	// render the component
	return (
		// if user is authenticated or an admin, this is accessible
		(user.id !== null || user.isAdmin === true) ?
			<Banner bannerProp={data} />
		:
		// else, redirect to 404 not found page
			<Navigate to="*" />
	)
}