// import built-in react modules
import { useState, useEffect, useContext }		from 'react';
import { useNavigate } 							from 'react-router-dom';
// import UserContext
import UserContext 								from '../UserContext';

// import components from react bootstrap
import { Container, Card, Button, Form, Modal } from 'react-bootstrap';
// import Swal
import Swal 									from 'sweetalert2';
// import components
import Footer 									from '../components/Footer';

// import images from assets folder
import ProfileImg 								from '../assets/intro-profile.jpg';
import ProfilePic 								from '../assets/profile.jpg'


// export the function so that it can be use anywhere
export default function Profile() {

	// set user from UserContext
	const { user } = useContext(UserContext);
	// set navigate to redirect to other page
	const navigate = useNavigate();

	// set states to hold the user data from fetch
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	// set states for changing the password
	const [oldPassword, setOldPassword] = useState('');
	const [newPassword1, setNewPassword1] = useState('');
	const [newPassword2, setNewPassword2] = useState('');

	// set boolean state that will enable the save button on conditional rendering
	const [isComplete, setIsComplete] = useState(false);

	// set states and function for the modal
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

  	// function for changing the password
  	function changePassword(e) {
  		// to prevent the page from refreshing/reloading
  		e.preventDefault();

  		fetch(`${process.env.REACT_APP_API_URL}/users/changePassword`, {
  			method: 'POST',
  			headers: {
  				'Content-Type': 'application/json',
  				Authorization: `Bearer ${localStorage.getItem('token')}`
  			},
  			body: JSON.stringify({
  				oldPassword: oldPassword,
  				newPassword: newPassword1
  			})
  		})
  		.then(res => res.json())
  		.then(data => {
  			if (data.message === "Your password is now updated") {
  				handleClose();
  				Swal.fire({
	               title: "Your password is now updated!",
	               icon: "success"
	           	})
  			} else {
  				handleClose();
  				Swal.fire({
	               title: "Old password did not match",
	               icon: "error",
	            })
  			}

  			// clear the fields
  			setOldPassword('');
  			setNewPassword1('');
  			setNewPassword2('');
  		});
  	}

  	// function to get the profile of the authenticated user
  	function fetchProfile(user) {
  		// condition to check if the user is authenticated and not an admin
  		if (user.id !== null && user.isAdmin === false) {
			fetch(`${process.env.REACT_APP_API_URL}/users/getUserDetails`, {
	            headers: {
	                Authorization: `Bearer ${localStorage.getItem('token')}`
	            }
	        })
	        .then(res => res.json())
	        .then(data => {
	        	// sets the states based from the fetched data from the database
	            setFirstName(data.details.firstName);
	            setLastName(data.details.lastName);
	            setEmail(data.details.email);
	            setMobileNo(data.details.mobileNo);
	        });

	    // if the user is an admin, redirect to forbidden page
		} else if (user.isAdmin === true) {
			navigate("/forbidden");

		// if the user is not authenticated, redirect to unauthorized
		} else if (localStorage.getItem('token') == null) {
			navigate("/unauthorized");
		}
  	}

  	// pass the fetchProfile function to run it at first page render
	useEffect(() => {
		fetchProfile(user);
	});
	// [] dependencies are optional

	/*
		this useEffect will check if the oldPassword field is filled 
		and the newPassword1 and newPassword2 are strictly equal to each other. 
		If yes setIsComplete will be true, otherwise set it to false
	*/
	useEffect(() => {
		if ((oldPassword !== "") && (newPassword1 === newPassword2)) {
			setIsComplete(true);
		} else {
			setIsComplete(false);
		}
	},[oldPassword, newPassword1, newPassword2]);

	return (
		<>
			{/* Modal for change password */}
			<Modal show={show} onHide={handleClose} centered className="text-white">
				<Modal.Header  className="primary-background" closeButton>
					<Modal.Title>Change password</Modal.Title>
				</Modal.Header>
				<Modal.Body className="primary-background">
					<Form onSubmit={e => changePassword(e)}>
						<Form.Group className="mb-3" controlId="oldPassword">
							<Form.Label>Old password</Form.Label>
							<Form.Control
							type="password"
							placeholder="Enter old password"
							value={oldPassword}
							onChange={e => setOldPassword(e.target.value)} 
							autoFocus
							/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="newPassword1">
							<Form.Label>New password</Form.Label>
							<Form.Control
							type="password"
							placeholder="Enter new password"
							value={newPassword1}
							onChange={e => setNewPassword1(e.target.value)}
							/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="newPassword2">
							<Form.Label>Confirm new password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Confirm your new password"
								value={newPassword2}
								onChange={e => setNewPassword2(e.target.value)} 
							/>
						</Form.Group>
						<div className="d-flex justify-content-end">
							<Button variant="danger" className="m-1" onClick={handleClose}>Cancel</Button>
							{ (isComplete) ? 
								<Button variant="info" className="m-1" type="submit">Save</Button>
								:
								<Button variant="info" className="m-1" disabled>Save</Button>
							}
						</div>
					</Form>
				</Modal.Body>
			</Modal>

			{/* Intro Image */}
			<div className="min-vh-100">
				<img src={ProfileImg} alt="intro_picture" className="intro-picture"/>
				<div className="overlay"></div>
				<div className="intro-text-profile content">				
		        	<div className="hero">		    
		        		<h2 className="title shadow-text">My Profile</h2>
		        	</div>
		    	</div>
			</div>
			
			{/* Profile body section */}
			<Container className="my-5 py-5">
				<div className="d-flex justify-content-center">
					<Card className="w-100 borderless card-shadow rounded-0 p-3">
						<div className="d-flex d-flex flex-column align-items-center">
							<Card.Img variant="top" src={ProfilePic} className="image-fit" />
							<Card.Body>
								<Card.Title className="text-center">Profile</Card.Title>
								<div className="d-flex justify-content-between">
									<Card.Text>Name:</Card.Text>
									<Card.Text>{`${firstName} ${lastName}`}</Card.Text>
								</div>								
								<div className="d-flex justify-content-between">
									<Card.Text>Email:</Card.Text>
									<Card.Text>{email}</Card.Text>
								</div>
								<div className="d-flex justify-content-between">
									<Card.Text>Mobile No. </Card.Text>
									<Card.Text>{mobileNo}</Card.Text>
								</div>
							</Card.Body>
						</div>
						<div className="d-flex justify-content-end">
							<Button className="btn btn-primary m-2" onClick={handleShow}>Change password</Button>
						</div>
					</Card>
				</div>
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>	
	)
}