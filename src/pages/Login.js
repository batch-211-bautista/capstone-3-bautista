// import built-in react modules
import { useContext } 											from 'react';
import { Navigate, Link } 										from 'react-router-dom';
// import UserContext
import UserContext 												from '../UserContext';

// import Swal
import Swal 													from 'sweetalert2';
// import formik
import { useFormik } 											from 'formik';

// import primereact dependencies
import { InputText } 											from 'primereact/inputtext';
import { Button } 												from 'primereact/button';
import { Password } 											from 'primereact/password';
import { classNames } 											from 'primereact/utils';

// import primeicons, primereact and primeflex dependencies
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/lara-light-teal/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';

// import components
import Footer 													from '../components/Footer';

// import image from assets folder
import LoginImg 												from '../assets/man-riding-mountain-bike-close-up.jpg';


// export the function so that it can be use anywhere
export default function Login() {

	// get user and setUser from UserContext
	const { user, setUser } = useContext(UserContext);

	// set the initial values on the fields and declare validations
	const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validate: (data) => {
            let errors = {};

            if (!data.email) {
                errors.email = 'Email is required.';
            }
            else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(data.email)) {
                errors.email = 'Invalid email address. E.g. example@email.com';
            }

            if (!data.password) {
                errors.password = 'Password is required.';
            }

            return errors;
        },
        // this serves as the submit function on formik
        onSubmit: (data) => {
        	// invoke the authenticate function
            authenticate();
            // clears the form upon submit
            formik.resetForm();
        }
    });

	// function for authenticating the user
	function authenticate() {
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: formik.values.email,
				password: formik.values.password
			})
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if email address is incorrect
			if (data.message === "Login failed: incorrect email address") {
				Swal.fire({
	               title: "Login failed",
	               icon: "error",
	               text: "incorrect email address"
	            })

	        // shows an alert if password is incorrect
			} else if (data.message === "Login failed: incorrect password") {
				Swal.fire({
	               title: "Login failed",
	               icon: "error",
	               text: "incorrect password"
	         	})

	        // shows an alert upon successful authentication
			} else if (typeof data.token.access !== "undefined") {
				localStorage.setItem('token', data.token.access);
				// invoke the retrieveUserDetails function
				retrieveUserDetails(data.token.access);
				Swal.fire({
		            title: "Login successful!",
		            icon: "success"
	            })
			} 
		});

		// function expression to get the details of the authenticated user
		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/getUserDetails`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// set the user's id and isAdmin property to the user on UserContext
				setUser({
					id: data.details._id,
					isAdmin: data.details.isAdmin
				});
			});
		};
	}

	// set variables that will hold the error messages for validation
	const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    // render the components
	return (
		// if the user is authenticated, redirect to products page
		(user.id !== null) ?
			<Navigate to="/products" />
		:
		<>
			{/* Intro Image */}
			<div className="pt-5">
				<img src={LoginImg} alt="register_picture" className="register-picture" />
				<div className="overlay"></div>
				<div className="intro-text content-login">
		        	<div className="hero">
		        		<h2 className="title shadow-text">Are you ready?</h2>
		        		<h2 className="sub-title">You're one step away from getting that upgrade for your bike</h2>
		        		<p className="description">Get access to bike parts and components from different known brands. Check out the latest Shimano Dura Ace Di2 groupset for your road bike or maybe just buy a new bike?</p>
		        	</div>
		    	</div>
			</div>
				
			{/* Login form from primereact */}
			<div className="form-demo my-5 py-5">
	            <div className="d-flex flex-column align-items-center align-items-lg-end register-login-card mx-3 my-5">
	                <div className="card p-4 rounded-0 borderless shadow-box bg-light">
	                    <h3 className="text-center my-2">Login</h3>
	                    <form onSubmit={formik.handleSubmit} className="p-fluid p-3 mt-2">
	                        <div className="field">
	                            <span className="p-float-label p-input-icon-right">
	                                <i className="pi pi-envelope" />
	                                <InputText id="email" name="email" value={formik.values.email} onChange={formik.handleChange} className={classNames({ 'p-invalid': isFormFieldValid('email') })} />
	                                <label htmlFor="email" className={classNames({ 'p-error': isFormFieldValid('email') })}>Email*</label>
	                            </span>
	                            {getFormErrorMessage('email')}
	                        </div>

	                        <div className="field">
	                            <span className="p-float-label">
	                                <Password id="password" name="password" value={formik.values.password} onChange={formik.handleChange} toggleMask
	                                    className={classNames({ 'p-invalid': isFormFieldValid('password') })} />
	                                <label htmlFor="password" className={classNames({ 'p-error': isFormFieldValid('password') })}>Password*</label>
	                            </span>
	                            {getFormErrorMessage('password')}
	                        </div>

	                        <Button type="submit" label="Login" className="mt-2" />
	                        <div className="d-flex justify-content-center my-3">
	                        	<span>Don't have an account yet? <Link to="/register">Sign up here</Link></span>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
			<Footer/>
		</>
	)
}