// import built-in react modules
import { useEffect, useState, useContext } 			from 'react';
import { useNavigate, Link } 						from 'react-router-dom';

// import UserContext
import UserContext 									from '../UserContext';

// import components from react bootstrap
import { Container, Button } 						from 'react-bootstrap';

// import components
import OrderCard 									from '../components/OrderCard';
import Footer 										from '../components/Footer';

// import image from assets folder
import CartImg 										from '../assets/intro-cart.jpg'


// export the function so that it can be use anywhere
export default function Cart() {

	// set state array to hold the cart details
	const [cart, setCart] = useState([]);

	// get user from UserContext
	const { user } = useContext(UserContext);

	// set navigate for navigating to other pages
	const navigate = useNavigate();

	// function for getting the cart data of the authenticated user
	function fetchCartData() {
		// condition to check if the user is authenticated and not an admin
		if (user.id !== null && user.isAdmin === false) {
			fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// if cart is empty, redirect to emptyCart page
				if (data.message === "Your cart is empty") {
					navigate("/emptyCart");

				// else proceed with the operation
				} else {
					/* ? (question mark) means it checks if data.cart is existing first
					before it executes the mapping */

					// use map to render OrderCard and return a new array assigned to cart variable
					let cart = (data.cart?.map(orders => {
						return <OrderCard orderProp={orders} isCartProp={true} key={orders.id}/>
					}));

					// set the cart state array
					setCart(cart);	
				}
			});

		// if user is an admin, redirect to forbidden page
		} else if (user.isAdmin === true) {
			navigate("/forbidden");

		// if user is not authenticated, redirect to unauthorized page
		} else if (localStorage.getItem('token') == null) {
			navigate("/unauthorized");
		}
	}

	// pass the fetchCartData on useEffect to run it at first page render
	useEffect(() => {
		fetchCartData();
	});

	// render the components
	return (
		<>
			{/* Intro Image */}
			<div className="min-vh-100">
				<div className="overlay"></div>
				<img src={CartImg} alt="intro_picture" className="intro-picture"/>				
				<div className="intro-text-profile content">				
		        	<div className="hero">		    
		        		<h2 className="title shadow-text">Shopping Cart</h2>
		        	</div>
		    	</div>
			</div>

			{/* Cart body section */}
			<Container className="my-4 py-4">
				<div className="d-flex justify-content-center justify-content-lg-end">
					<Button as={Link} to="/products" className="cta mb-3 py-2 pe-1 primary-background">
					    <span className="hover-underline-animation">Continue shopping</span>
					    <i className='bx bx-right-arrow-alt continue-arrow'></i>
					</Button>
				</div>
				{cart}
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>
	)
}