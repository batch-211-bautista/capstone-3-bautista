// import components
import Banner from '../components/Banner';

// import gif from assets folder
import NotFound from '../assets/404 error with people holding the numbers.gif';

// export the function so that it can be use anywhere
export default function ErrorPage() {

	/* declare an object to hold the details 
	to be passed as props to the banner component */
	const data = {
		source: `${NotFound}`,
		alt: "404_not_found",
		submessage: "The page you are looking for cannot be found",
		label: "Return",
		route: "/"
	}

	// render the component
	return (
		<Banner bannerProp={data} />
	)
}