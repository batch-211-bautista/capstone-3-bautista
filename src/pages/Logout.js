// import built-in react modules
import { useContext, useEffect } 		from 'react';
import { Navigate } 					from 'react-router-dom';

// import UserContext
import UserContext 						from '../UserContext';


// export the function so that it can be use anywhere
export default function Logout() {

	// get unsetUser and setUser from UserContext
	const { unsetUser, setUser } = useContext(UserContext);

	// invoke unsetUser to clear the localStorage
	unsetUser();

	// pass setUser on useEffect to run it at first page render
	useEffect(() => {
		// set the user id to null
		setUser({id: null});
	});

	return (
		// Navigate back to home page
		<Navigate to="/" />
	)
}