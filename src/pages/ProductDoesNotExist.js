// import built-in react modules
import { Navigate } 		from 'react-router-dom';
import { useContext } 		from 'react';
import UserContext 			from '../UserContext';

// import components
import Banner 				from '../components/Banner';

// import gif from assets folder
import ProductNonExistent  from '../assets/Mystery box.gif';

// export the function so that it can be use anywhere
export default function ProductDoesNotExist() {

	// get the user from the UserContext
	const { user } = useContext(UserContext);
	
	/* declare an object to hold the details 
	to be passed as props to the banner component */
	const data = {
		message: "Product does not exist",
		source: `${ProductNonExistent}`,
		alt: "product_does_not_exist",
		label: "Return",
		route: "/products"
	}

	// render the component
	return (
		// if user is authenticated, this is accessible
		(user.id !== null) ?
			<Banner bannerProp={data} />
		:
		// else, redirect to 404 not found page
			<Navigate to="*" />
	)
}