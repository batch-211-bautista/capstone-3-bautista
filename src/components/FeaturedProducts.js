// import built-in react modules
import { Link } 		from 'react-router-dom';

// import component from react bootstrap
import { Button } 		from 'react-bootstrap';


// export the function so that it can be use anywhere
export default function FeaturedProducts() {

	// render the component
	return (
		<div className="sm-container" id="featured-products">
			<h2 className="title-new">Featured products</h2>
			<div className="row">
				<div className="col-2 card-layout">
					<img src={"https://i.ibb.co/THNv6D7/shimano-xc1-removebg-preview.png"} alt="shimano_xc1_mountain_bike_shoes" />
					<h4>Shimano XC1 Mountain Bike shoes</h4>
					<span>Ratings</span>
					<div className="rating">
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star-half'></i>
						<i className='bx bx-star' ></i>
					</div>
					<div className="d-flex justify-content-center">
						<Button className="my-1 more-details"  as={Link} to="/products/63904850d5002a4a038466ca">Learn more</Button>
					</div>
				</div>
				<div className="col-2 card-layout">
					<img src={"https://i.ibb.co/1vZ7xVy/specialized-recon-2-removebg-preview.png"} alt="specialized_recon_mountain_bike_shoes" />
					<h4>Specialized Recon 2.0 Mountain Bike shoes</h4>
					<span>Ratings</span>
					<div className="rating">
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star-half'></i>
					</div>
					<div className="d-flex justify-content-center">
						<Button className="my-1 more-details" as={Link} to="/products/63904850d5002a4a038466cb">Learn more</Button>
					</div>
				</div>
				<div className="col-2 card-layout">
					<img src={"https://i.ibb.co/n1z2xYS/sram-gx-eagle-removebg-preview.png"} alt="sram_gx_eagle_groupset" />
					<h4>Sram GX Eagle groupset</h4>
					<span>Ratings</span>
					<div className="rating">
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
					</div>
					<div className="d-flex justify-content-center">
						<Button className="my-1 more-details" as={Link} to="/products/63904850d5002a4a038466c9">Learn more</Button>
					</div>
				</div>
				<div className="col-2 card-layout">
					<img src={"https://i.ibb.co/t23gKfD/shimano-XT-8000.png"} alt="shimano_deore_groupset" />
					<h4>Shimano Deore groupset</h4>
					<span>Ratings</span>
					<div className="rating">
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bxs-star'></i>
						<i className='bx bx-star' ></i>
					</div>
					<div className="d-flex justify-content-center">
						<Button className="my-1 more-details" as={Link} to="/products/63904837d5002a4a038466c6">Learn more</Button>
					</div>
				</div>
			</div>
		</div>
	)
}