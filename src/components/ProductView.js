// import built-in react modules
import { useParams, useNavigate, Link, Navigate } 			from 'react-router-dom';
import { useState, useEffect, useContext } 					from 'react';
import UserContext 											from '../UserContext';

// import components from react bootstrap
import { 
	Container,
	Card,
	Button,
	Offcanvas,
	Form,
	OverlayTrigger,
	Tooltip
} 															from 'react-bootstrap';
// import Swal
import Swal 												from 'sweetalert2';
// import component
import Footer 												from '../components/Footer';


// export the function so that it can be use anywhere
export default function ProductView() {

	/* get user, orderDetails, setOrderDetails, setAddToCartState 
	and setBuyNowState from UserContext */
	const { 
		user,
		orderDetails,
		setOrderDetails,
		setAddToCartState,
		setBuyNowState 
	} = useContext(UserContext);

	// get the productId from the url parameter
	const { productId } = useParams();

	// set navigate for navigating to other pages
	const navigate = useNavigate();

	// set states for the product details
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [source, setSource] = useState('');
	const [quantity, setQuantity] = useState('');

	// set boolean state for conditional rendering of buttons
	const [isAddToCart, setIsAddToCart] = useState(false);

	// set boolean state for conditional rendering of buttons
	const [isEditable, setIsEditable] = useState(false);

	// set boolean state for conditional rendering of add new button
	const [addNew, setAddNew] = useState(false);

	// handle function once add to cart button is clicked on product view
	const addToCartHandle = () => {
		handleShow();
		setIsAddToCart(true);
		setAddToCartState(true);
	}

	// handle function once buy now button is clicked on product view
	const buyNowHandle = () => {
		handleShow();
		setIsAddToCart(false);
		setBuyNowState(true);
	}

	// set state for offcanvas controls
	const [show, setShow] = useState(false);

	// handle function on offcanvas close
	const handleClose = () => {
		setShow(false);
		setQuantity('');
		setIsEditable(false);
	}

	// handle function on offcanvas show
  	const handleShow = () => setShow(true);

  	// handle function once add new button is clicked
  	const handleOnChange = (e) => {
  		// push the productId and quantity of the selected product to the orderDetails state array
  		setOrderDetails(current => [...current, {productId: `${productId}`, quantity: `${quantity}`}])
  		Swal.fire({
	      	title: "Quantity saved",
	      	icon: "success"
		})
  	}

  	// handle function once save quantity button is clicked
  	const handleQuantityChange = (e) => {
  		setQuantity(e.target.value);
  		setAddNew(true);
  	}

  	// handle function to change the isEditable state to it's reverse current state
  	const handleEditButton = () => {
  		setIsEditable(!isEditable);
  	}

  	// handle function on navigating to products page
  	const handleNavigateToProducts = () => {
  		navigate("/products");
  	}

  	// function for creating an order
  	function createOrder(e, orderDetails) {
  		// use preventDefault to prevent the page from refreshing/reloading
  		e.preventDefault();

  		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
  			method: 'POST',
  			headers: {
  				'Content-Type': 'application/json',
  				Authorization: `Bearer ${localStorage.getItem('token')}`
  			},
  			body: JSON.stringify(orderDetails)
  		})
  		.then(res => res.json())
  		.then(data => {
  			handleClose();
  			// shows an alert once operation is successful
  			if (data.message === "Order created") {
  				Swal.fire({
		         	title: "Order created successfully!",
		         	icon: "success"
	         	})
	  			// reset the orderDetails back to an empty array
	  			setOrderDetails([]);
	  			// navigate to order history
	            navigate("/orders");

	        // shows an alert if operation failed
  			} else {
  				Swal.fire({
		            title: "Operation failed",
		            icon: "error",
		        })
	  			// reset the orderDetails back to an empty array
		        setOrderDetails([]);
  			}

  			// reset the quantity field
  			setQuantity('');
  			// set the buyNowState to false again
  			setBuyNowState(false);
  		});
  	}

  	// function for adding to cart
  	function addToCart(e, orderDetails) {
  		// use preventDefault to prevent the page from refreshing/reloading
  		e.preventDefault();

  		fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`, {
  			method: 'POST',
  			headers: {
  				'Content-Type': 'application/json',
  				Authorization: `Bearer ${localStorage.getItem('token')}`
  			},
  			body: JSON.stringify(orderDetails)
  		})
  		.then(res => res.json())
  		.then(data => {
  			handleClose();
  			// shows an alert once operation is successful
  			if (data.message === "Items added to cart") {
  				Swal.fire({
		         	title: "Item added to cart!",
		         	icon: "success"
	        	})
	        	// reset the orderDetails back to an empty array
  				setOrderDetails([]);
  				// navigate to cart
            	navigate("/cart");
  			} else {
				Swal.fire({
	            	title: "Operation failed",
	            	icon: "error",
	         	})
	         	// reset the orderDetails back to an empty array
  				setOrderDetails([]);
  			}

  			// reset the quantity field
  			setQuantity('');
  			// set the addToCartState to false again
  			setAddToCartState(false);
  		});
  	}

  	// function for formatting the price, subtotal and totalAmount
	function formatPrice(price) {
		return price.toLocaleString(undefined, { style: 'currency', currency: 'PHP' })
	}

  	// fetch the details of the current product at first page render
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// condition to check if the product does not exist
			if (data.message === "Product does not exists") {
				// if yes, redirect to noProduct page
				navigate("/noProduct");

			// else proceed with the operation
			} else {
				setName(data.product.name);
				setDescription(data.product.description);
				setPrice(data.product.price);
				setSource(data.product.source);
			}

		});
	},[productId, navigate]);

	// render the components
	return (
		<>
			{/* Offcanvas for add to cart/buy now */}
			<Offcanvas show={show} onHide={() => handleClose()} placement={"bottom"} className="h-auto p-3 primary-background text-white">
				<Offcanvas.Header className="ps-4 py-1" closeButton>
					<Offcanvas.Title>{ isAddToCart ? "Add To Cart" : "Checkout"}</Offcanvas.Title>
				</Offcanvas.Header>
				<Offcanvas.Body className="primary-background">
					<Card>
						<Card.Body className="primary-background">
							<div className="d-flex flex-column flex-lg-row justify-content-lg-start gap-4">
								<div className="bg-white rounded product-view-offcanvas mx-sm-auto mx-lg-0 mb-0 mb-lg-3 p-2">
									<img src={source} alt="product_image" className="img-fluid"/>
								</div>
								<div className="d-flex flex-column justify-content-center">
									<Card.Title className="mb-3">{name}</Card.Title>
									<Card.Subtitle>Description</Card.Subtitle>
									<Card.Text><span className="product-description">{description}</span></Card.Text>
									<Card.Subtitle>Price</Card.Subtitle>
									<Card.Text>{formatPrice(price)}</Card.Text>
								</div>
							</div>
							<Form>
								<div className="d-flex flex-column flex-md-row justify-content-start gap-2">
									{ (isEditable) ? 
										<>
											<Form.Group className="quantity-input" controlId="productPrice">
												<Form.Label><strong>Quantity</strong></Form.Label>
												<Form.Control 
													type="number" 
													min={1}
													max={100}
													step={1}
													value={quantity}
													placeholder="Enter a number"
													onChange={(e) => handleQuantityChange(e)} 
												/>
											</Form.Group>
										</>
										:
										<>
											<Form.Group className="quantity-input" controlId="productPrice">
												<Form.Label><strong>Quantity</strong></Form.Label>
												<Form.Control 
													type="number" 
													min={1}
													max={100}
													step={1}
													placeholder="Enter a number"
													disabled
												/>
											</Form.Group>
										</>
									}
									<OverlayTrigger placement={"top"} overlay={<Tooltip><strong>Edit</strong> quantity</Tooltip>}>
										<Button variant="success" className="align-self-end h-25 edit-quantity-btn" onClick={() => handleEditButton()}><i className='bx bx-edit'></i> <span className="edit-quantity">Edit quantity</span></Button>
									</OverlayTrigger>

									{ isEditable && <Button variant="primary" className="align-self-end offwhite save-quantity-btn h-25" onClick={(e) => handleOnChange(e)}><i className='bx bx-save'></i> Save quantity</Button>}
							
									{ (isAddToCart === true) ? 
										(isEditable) ?
											<>
												{ (addNew) && <Button variant="primary" className="align-self-end me-auto offwhite add-new-btn h-25" onClick={(e) => handleNavigateToProducts(e)}><i className='bx bx-plus'></i> Add new</Button>}
												<Button variant="info" className="align-self-end add-to-cart-btn h-25" onClick={(e) => addToCart(e, orderDetails)}><i className='bx bx-cart-alt'></i> Add to cart</Button>
											</>
											:
											<Button variant="info" className="align-self-end add-to-cart-btn h-25" disabled><i className='bx bx-cart-alt'></i> Add to cart</Button>
										:
										(isEditable) ?
											<>
												{ (addNew) && <Button variant="primary" className="align-self-end me-auto offwhite add-new-btn h-25" onClick={(e) => handleNavigateToProducts(e)}><i className='bx bx-plus'></i> Add new</Button>}
												<Button variant="info" className="align-self-end checkout-btn h-25" onClick={(e) => createOrder(e, orderDetails)}><i className='bx bx-check-square'></i> Checkout</Button>
											</>
											:
											<Button variant="info" className="align-self-end checkout-btn h-25" disabled><i className='bx bx-check-square'></i> Checkout</Button>
									}
								</div>
							</Form>
						</Card.Body>
					</Card>
				</Offcanvas.Body>
			</Offcanvas>

			{/* if user is an admin, redirect to forbidden page */}
			{ (user.isAdmin) ? 
				<Navigate to="/forbidden" />
				:
				<>
					{/* Product view section */}
					<Container className="my-5 py-5">
						<div className="d-flex justify-content-end mx-1 mx-md-0">
							<Button as={Link} to="/products" className="mb-3"><i className='bx bx-arrow-back'></i></Button>
						</div>
					    <div className="d-flex justify-content-center align-items-center my-2">
				         	<Card className="borderless shadow-box rounded-0 p-3 product-view-card">
					            <Card.Body>
					            	<div className="d-flex product-view-image mx-auto">
					            		<img src={source} alt="product_image" className="img-fluid"/>
					            	</div>
					            	<Card.Title className="mb-3">{name}</Card.Title>
					            	<Card.Subtitle>Description</Card.Subtitle>
					            	<Card.Text><span className="product-description">{description}</span></Card.Text>
					            	<Card.Subtitle>Price</Card.Subtitle>
					            	<Card.Text>{formatPrice(price)}</Card.Text>
					            	<div className="d-flex justify-content-center justify-content-lg-end">
						            	{ (user.id !== null) ?
							              	<>
							              		<Button variant="primary" className="offwhite m-2" onClick={() => addToCartHandle()}><i className='bx bx-cart-alt'></i> Add to cart</Button>
								              	<Button variant="primary" className="offwhite m-2" onClick={() => buyNowHandle()}><i className='bx bx-check-square'></i> Buy now</Button>
							              	</>
						              	:
						              		<Button as={Link} to="/login" variant="primary" className="offwhite">Log in to continue</Button>
						              	}
					               </div>
					            </Card.Body>
				          	</Card>
					    </div>
			    	</Container>

			    	{/* Fixed bottom footer */}
		    		<Footer/>
		    	</>
			}
	   </>
	)
}