// import built-in react modules
import { Link } 				from 'react-router-dom';
import { useContext } 			from 'react';
import UserContext 				from '../UserContext';

// import components from react bootstrap
import { Card, Button } 		from 'react-bootstrap';
// import Swal
import Swal 					from 'sweetalert2';


// export the function so that it can be use anywhere
export default function ProductCard({ productProp }) {

	// desctructure the productProp
	const { name, price, source, _id, isActive } = productProp;

	// get user from UserContext
	const { user } = useContext(UserContext);

	// function for archiving a product
	function archiveProduct(e) {
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successfull
			if (data.message === "Product archived successfully!") {
				Swal.fire({
					title: 'Archived!',
			    	text: 'Your product has been archived.',
			      	icon: 'success'
				})

			// shows an alert if operation failed
			} else {
				Swal.fire({
		            title: "Operation failed",
		            icon: "error"
	    		})
			}
		});
	}

	// function for activating a product
	function activateProduct(e) {
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successfull
			if (data.message === "Product activated successfully!") {
				Swal.fire({
					title: 'Activated!',
			      	text: 'Your product has been activated.',
			      	icon: 'success'	
				})

			// shows an alert if operation failed
			} else {
				Swal.fire({
	           		title: "Operation failed",
	           		icon: "error"
		     	})
			}
		});
	}

	// function for formatting the price, subtotal and totalAmount
	function formatPrice(price) {
		return price.toLocaleString(undefined, { style: 'currency', currency: 'PHP' })
	}

	// render the component
	return (
		<Card className="borderless product-card-layout rounded-0 mb-3 p-3">
			<Card.Img variant="top" src={source} />
		    <Card.Body className="d-flex flex-column justify-content-center mb-3">
		    	<Card.Title>{name}</Card.Title><br/>
	         	<Card.Subtitle>Price</Card.Subtitle>
	         	<Card.Text>{formatPrice(price)}</Card.Text>
	         	<div className="d-flex justify-content-center">
		         	{/* Learn more button will only show for authenticated and not authenticated normal user */}
				   	{ (user.isAdmin === false || user.id === null) ? 
				        	<Button as={Link} to={`/products/${_id}`} variant="primary" className="details-btn offwhite">Learn more</Button>
				        	:
				        	<div className="admin-btn">
					        	<Button as={Link} to={`/products/edit/${_id}`} variant="success" className="m-1"><i className='bx bx-edit'></i> Edit</Button>
					        	{ (isActive) ? 
					        		<Button as={Link} variant="danger" className="m-1" onClick={e => archiveProduct(e)}><i className='bx bx-archive-in'></i> Archive</Button>
					        		:
					        		<Button as={Link} variant="info" className="m-1" onClick={e => activateProduct(e)}><i className='bx bx-archive-out'></i> Activate</Button>
					        	}
				        	</div>
				    }
		      </div>
	    	</Card.Body>
    	</Card>
	)
}