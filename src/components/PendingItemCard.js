// export the function so that it can be use anywhere
export default function PendingItemCard({pendingItemProp, pendingQuantityProp}) {

	// desctructure the pendingItemProp
	const { name, price } = pendingItemProp.product;

	// function for formatting the price
	function formatPrice(price) {
		return price.toLocaleString(undefined, { style: 'currency', currency: 'PHP' })
	}

	// render the component
	return (
		<div className="d-flex flex-column bg-light text-primary my-2 p-3">
			<h6>Name: {name}</h6>
			<h6>Price: {formatPrice(price)}</h6>
			<h6 className="mb-0">Quantity: {pendingQuantityProp}</h6>
		</div>
	)
}