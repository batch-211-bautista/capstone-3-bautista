// import built-in react modules
import { NavLink }                  from 'react-router-dom';
import { useContext, useState }     from 'react';

// import UserContext
import UserContext                  from '../UserContext';

// import components from react bootstrap
import { Button, Offcanvas }        from 'react-bootstrap';

// import images from assets folder
import Bikemeds                     from '../assets/bikemeds.png';
import Menu                         from '../assets/menu.png';


// export the function so that it can be use anywhere
export default function AppNavbar() {

   // get the user from UserContext
   const { user } = useContext(UserContext);

   // set boolean state for conditional rendering
   const [visible, setVisible] = useState(false);

   // set boolean state for conditional rendering
   const [show, setShow] = useState(false);

   // handle function for offcanvas close
   const handleClose = () => setShow(false);

   // handle function for offcanvas show
   const handleShow = () => setShow(true);

   // handle function for reversing visible current state
   const handleVisible = () => setVisible(!visible);

   // render the components
	return (
      <>
         {/* Sidebar section */}
         <Offcanvas show={show} onHide={handleClose} scroll={true} backdrop={true} className="primary-background">
           <Offcanvas.Header className="d-flex justify-content-between" closeButton>
              <Offcanvas.Title>
                  <img src={Bikemeds} alt="bikemeds_logo" className="bikemeds-logo"/>
              </Offcanvas.Title>
            </Offcanvas.Header>
              <Offcanvas.Body className="p-0">
                 <div className="sidebar">
                     <ul>
                        { (user.isAdmin === false) ? 
                           <>
                              <li><NavLink className="sidebar-link" to="/profile" onClick={handleClose}><i className='bx bx-user-circle sidebar-icons'></i><span>Profile</span></NavLink></li>
                              <li><NavLink className="sidebar-link" to="/products" onClick={handleClose}><i className='bx bx-package sidebar-icons'></i><span>Products</span></NavLink></li>
                              <li><NavLink className="sidebar-link" to="/cart" onClick={handleClose}><i className='bx bx-cart sidebar-icons'></i><span>Cart</span></NavLink></li>
                              <li><NavLink className="sidebar-link" to="/orders" onClick={handleClose}><i className='bx bx-list-check sidebar-icons'></i><span>Order history</span></NavLink></li>
                           </>
                           :
                           <>
                              <li><NavLink className="sidebar-link" to="/products" onClick={handleClose}><i className='bx bx-package sidebar-icons'></i><span>Products</span></NavLink></li>
                              <li><NavLink className="sidebar-link" to="/admin/users" onClick={handleClose}><i className='bx bx-user sidebar-icons'></i><span>Users</span></NavLink></li>
                              <li><NavLink className="sidebar-link" to="/admin/orders" onClick={handleClose}><i className='bx bx-list-check sidebar-icons'></i><span>All Orders</span></NavLink></li>
                           </>
                        }
                     </ul>
               </div>
            </Offcanvas.Body>
         </Offcanvas>

         {/* Navbar section */}
         <div className={ user.id !== null ? "navbar-authenticated fixed-top" : "navbar-css fixed-top"}>
            { (user.id !== null) && <Button variant="primary" onClick={handleShow} className="pb-0 primary-background borderless shadow-box"><i className='bx bx-menu sidebar-menu'></i></Button> }
            <div>
               <img src={Bikemeds} alt="bikemeds_logo" className={ user.id !== null ? "bikemeds-logo-authenticated" : "bikemeds-logo"}/>
            </div>
            <nav>
               <ul className={ visible === true ? "visible-ul" : "" }>
                  { (user.id !== null) ?    
                     <li><NavLink to="/logout" onClick={handleVisible}>Logout</NavLink></li>
                  :
                     <>
                        <li><NavLink to="/" onClick={handleVisible}>Home</NavLink></li>
                        <li><NavLink to="/products" onClick={handleVisible}>Products</NavLink></li>
                        <li><NavLink to="/login" onClick={handleVisible}>Login</NavLink></li>
                        <li><NavLink to="/register" onClick={handleVisible}>Sign up</NavLink></li>
                     </>
                  }
               </ul>
            </nav>
            { (user.id === null) ? 
                  <img src={Menu} className="menu-icon" onClick={handleVisible} alt="menu_icon" />
               :
                  <Button className="primary-background borderless shadow-box logout-button" onClick={handleVisible} alt="logout_button"><i className='bx bx-log-out-circle logout-icon'></i></Button>
            }
            
         </div>
      </>
	)
}