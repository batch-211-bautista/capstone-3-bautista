// export the function so that it can be use anywhere
export default function PaginationComp({totalPosts, postsPerPage, setCurrentPage, currentPage}) {

	// this array will contain the pagination numbers
	let pages = [];

	// logic to get the pagination numbers based from totalPosts and postsPerPage
	for (let i = 1; i <= Math.ceil(totalPosts/postsPerPage); i++) {
		pages.push(i);
	}

	// render the component
	return(
		<div className="d-flex justify-content-center my-3">
			{
				pages.map((page, index) => {
					return (
						<button 
							key={index} 
							onClick={() => setCurrentPage(page)}
							className={ page === currentPage ? "pagination-btn-active" : "pagination-btn"}
						>
							{page}
						</button>
					)
				})
				
			}
		</div>
	)
}