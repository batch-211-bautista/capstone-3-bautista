// import images from assets folder
import Shimano 		from '../assets/shimano-logo.png';
import Sram 		from '../assets/sram-logo.png';
import Specialized  from '../assets/specialized-logo.png';
import Maxxis 		from '../assets/maxxis-logo.png';
import Spyder 		from '../assets/spyder-philippines.png';


// export the function so that it can be use anywhere
export default function FeaturedBrands() {

	// render the component
	return (
		<div className="brands">
		    <div className="sm-container">
		    	<h2 className="title-new">Featured Brands</h2>
		        <div className="row">
		            <div className="col-5">
		               <img src={Shimano} alt="shimano_logo"/>
		            </div>
		            <div className="col-5">
		               <img src={Sram} alt="sram_logo"/>
		            </div>
		            <div className="col-5">
		               <img src={Spyder} alt="controltech_logo"/>
		            </div>
		            <div className="col-5">
		               <img src={Specialized} alt="specialized_logo"/>
		            </div>
		            <div className="col-5">
		               <img src={Maxxis} alt="maxxis_logo"/>
		            </div>
		        </div>
		    </div>
		</div>
	)
}