// export the function so that it can be use anywhere
export default function LandingPageText() {

	// render the component
	return (
		<div className="content" id="top">
        	<div className="hero">
        		<h2 className="title shadow-text">Feeling Bored?<br/>Ride a Bike</h2>
        		<h2 className="sub-title">Sign up now to check out exclusive bike products</h2>
        		<p className="description">Get access to bike parts and components from different known brands. Check out the latest Shimano Dura Ace Di2 groupset for your road bike or maybe just buy a new bike?</p>
        		<a href="#featured-products"><button className="shop-now">Shop now</button></a>
        	</div>
      </div>
	)
}