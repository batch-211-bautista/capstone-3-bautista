// import built-in react modules
import { useNavigate } 				from 'react-router-dom';
import { useState, useEffect } 		from 'react';

// import component from react bootstrap
import { Button } 					from 'react-bootstrap';
// import Swal
import Swal 						from 'sweetalert2';

// import component
import OrderCardSubComp 			from './OrderCardSubComp';


// export the function so that it can be use anywhere
export default function OrderCard({orderProp, isCartProp}) {

	// destructure the orderProp
	const { totalAmount, purchasedOn, products, id } = orderProp;
	
	// set orders state array to store the orders data
	const [orders, setOrders] = useState([]);

	// set navigate for navigating to other pages
	const navigate = useNavigate();

	// function to remove group of products from cart
	function removeGroupOfProducts(e) {
		// use preventDefault to prevent the page from refreshing/reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/cart/remove/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successfull
			if (data.message === "Products successfully removed from cart") {
				Swal.fire({
					title: "Products successfully removed from cart!",
					icon: "success"
				})

			// shows an alert if operation failed
			} else {
				Swal.fire({
					title: "Operation failed",
					icon: "error"	                
				})
			}
		});
	}

	// function for checking out a product from cart
	function checkoutFromCart(e) {
		// use preventDefault to prevent the page from refreshing/reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkoutFromCart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				orderId: id
			})
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successfull
			if (data.message === "Items successfully checkout") {
				Swal.fire({
					title: "Item successfully checked out",
					icon: "success"
				})
				// navigate to order history
				navigate("/orders")

			// shows an alert if operation failed
			} else {
				Swal.fire({
					title: "Operation failed",
					icon: "error"	                
				})
			}
		});
	}

	// function for formatting the date
	function formatDate(purchaseDate) {
		return (new Date(purchaseDate)).toLocaleString(
			'en-PH',
			{
				year: 'numeric',
				month: 'short',
				day: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
				second: 'numeric',
				hour12: true
			}
		)
	}

	// function for formatting the price, subtotal and totalAmount
	function formatPrice(price) {
		return price.toLocaleString(undefined, { style: 'currency', currency: 'PHP' })
	}

	// render the OrderCardSubComp upon first page render
	useEffect(() => {
		// use map to render OrderCardSubComp and return a new array assigned to orders variable
		let orders = products.map((product, index) => {
			return (
				<OrderCardSubComp dataProp={product} isCartProp={isCartProp} key={index}/>
			)
		});

		// set orders state array
		setOrders(orders);

	// set dependencies
	},[products, isCartProp]);

	// render the components
	return (
		<div className="p-4 mb-3 shadow-box bg-light">
			<div className="d-flex flex-column flex-lg-row justify-content-between gap-3">
				<div className="d-flex flex-column mx-auto w-75">
					{!isCartProp && <small><p className="align-self-center align-self-lg-start text-muted text-truncate mb-1 mx-2">Order no. {id}</p></small>}
					{orders}
				</div>
				<div className="d-flex flex-column justify-content-between">
					{!isCartProp && <h6 className="align-self-center m-2">Placed on: {formatDate(purchasedOn)}</h6>}
					<h5 className="m-2 align-self-center align-self-lg-end">Total: {formatPrice(totalAmount)}</h5>
				</div>
			</div>
			<div className="d-flex justify-content-center justify-content-lg-end gap-2 my-1">
				{ (isCartProp) &&
					<>
						<Button className="btn btn-danger btn-sm" onClick={e => removeGroupOfProducts(e)}><i className='bx bx-trash'></i> Remove</Button>
						<Button className="btn btn-primary btn-sm" onClick={e => checkoutFromCart(e)}><i className='bx bx-check-square'></i> Checkout</Button>
					</> }
			</div>
		</div>
	)
}