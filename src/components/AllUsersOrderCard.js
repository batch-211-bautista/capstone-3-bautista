// import built-in react modules
import { useEffect, useState } from 'react';


// export the function so that it can be use anywhere
export default function AllUsersOrderCard({orderProp}) {

	// set productData state array to store product details of all user's order
	const [productData, setProductData] = useState([]);

	// desctructure the orderProp
	const { id, name, products, purchasedOn, totalAmount } = orderProp;

	// function for formatting the date
	function formatDate(purchaseDate) {
		return (new Date(purchaseDate)).toLocaleString(
			'en-PH',
			{
				year: 'numeric',
				month: 'short',
				day: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
				second: 'numeric',
				hour12: true
			}
		)
	}

	// function for formatting the price, subtotal and totalAmount
	function formatPrice(price) {
		return price.toLocaleString(undefined, { style: 'currency', currency: 'PHP' })
	}

	// pass the mapping of products on useEffect to run it at first page render
	useEffect(() => {
		// use map to render a subcomponent and return a new array assigned to productDatas variable
		let productDatas = products.map(data => {
			return (
				<div key={data._id} className="d-flex primary-background text-white my-2 p-4 gap-4">
					<div className="d-flex flex-column justify-content-evenly">
						<h6>Name: {data.name}</h6>
						<h6>Quantity: {data.quantity}</h6>
						<h6>Price: {formatPrice(data.price)}</h6>
						<h6 className="mb-0">Subtotal: {formatPrice(data.subtotal)}</h6>
					</div>
				</div>
			)
		});

		// set productData state array
		setProductData(productDatas);

	// set dependencies
	},[products]);

	// render the component
	return (
		<div key={id} className="bg-light shadow-box p-4 mb-3">
			<div className="d-flex flex-column flex-lg-row justify-content-lg-between">
				<h5 className="align-self-center align-self-lg-start">Customer Name: {name}</h5>
				<div className="d-flex flex-column align-self-center align-self-lg-end gap-1">
					<small><span className="text-muted mb-1">Order number: {id}</span></small>
					<h6>Purchase Date: {formatDate(purchasedOn)}</h6>
				</div>
			</div>
			<div>
				{productData}
			</div>
			<div className="d-flex justify-content-center justify-content-lg-end mt-3">
				<h5>Total: {formatPrice(totalAmount)}</h5>
			</div>
		</div>
	)
}