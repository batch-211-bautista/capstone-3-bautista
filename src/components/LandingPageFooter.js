// import built-in react modules
import { Link } 		from 'react-router-dom';

// import images from assets folder
import PlayStore 		from '../assets/play-store.png';
import AppStore 		from '../assets/app-store.png';
import Bikemeds 		from '../assets/bikemeds.png';


// export the function so that it can be use anywhere
export default function LandingPageFooter() {

	// render the component
	return (
		<div className="footer">
		    <div className="container">
		        <div className="row">
		            <div className="footer-col-1">
		                <h3>Download our App</h3>
		                <p>Download our app for Android and ios mobile phone.</p>
		                <div className="app-logo">
		                	<img src={PlayStore} alt="playstore_logo"/>
		                    <img src={AppStore} alt="appstore_logo"/>
		                </div>
		            </div>
		            <div className="footer-col-2">
		            	<img src={Bikemeds} alt="bikemeds_logo" />
		                <p>Our purpose is to provide accessibility to bike parts and components from different known brands and also to expand the cycling community by encouraging people to ride a bike.</p>
		            </div>
		            <div className="footer-col-3">
		                <h3>Useful Links</h3>
		                <ul>
		                	<li><a className="useful-links" href="#top">Home</a></li>		                    
	                        <li><Link className="useful-links" to="/products">Products</Link></li>
	                        <li><Link className="useful-links" to="/login">Login</Link></li>
	                        <li><Link className="useful-links" to="/register">Sign up</Link></li>
		                </ul>
		            </div>
		            <div className="footer-col-4">
		                <h3>Follow us</h3>
		                <ul>
		                	<li className="useful-links">Facebook</li>
		                	<li className="useful-links">Twitter</li>
		                	<li className="useful-links">Instagram</li>
		                	<li className="useful-links">YouTube</li>
		                </ul>
		            </div>
		        </div>
		        <hr/>
		        <p className="copyright">Copyright &copy; 2022 BikeMeds. All rights reserved.</p>
		    </div>
		</div>
	)
}