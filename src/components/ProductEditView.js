// import built-in react modules
import { useState, useEffect, useContext } 		from 'react';
import { useParams, useNavigate, Link } 		from 'react-router-dom';
// import UserContext
import UserContext 								from '../UserContext';

// import components from react bootstrap
import { Container, Button, Form } 				from 'react-bootstrap';
// import Swal
import Swal 									from 'sweetalert2';

// import components
import Footer 									from '../components/Footer';


// export the function so that it can be use anywhere
export default function ProductEditView() {

	// get the productId from the url parameter
	const { productId } = useParams();

	// set navigate for navigating to other pages
	const navigate = useNavigate();

	// set states for product details
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [source, setSource] = useState('');

	// set boolean state for conditional rendering of save button
	const [isActive, setIsActive] = useState(false);

	// get user from UserContext
	const { user } = useContext(UserContext);

	// function for updating the product details
	function editProduct(e) {
		// use preventDefault to prevent the page from refreshing/reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				source: source
			})
		})
		.then(res => res.json())
		.then(data => {
			// if product does not exist, redirect to noProduct page
			if (data.message === "Operation failed: product does not exists") {
				navigate("/noProduct");

			// shows an alert if operation is successful
			} else if (data.message === "Product updated successfully!") {
				Swal.fire({
	               title: "Product updated successfully!",
	               icon: "success"
	           	})

	        // shows an alert if operation failed
			} else {
				Swal.fire({
	               title: "Operation failed",
	               icon: "error",
	               text: "Cannot update product"
	            })
			}
			// navigate to products
			navigate("/products");
		});
		// clears the name, description, price and source fields
		setName('');
		setDescription('');
		setPrice('');
		setSource('');
	}

	// fetch the product details on first page render
	useEffect(() => {
		// condition to check if user is authenticated and is an admin
		if (user.id !== null && user.isAdmin === true) {
			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				// if product does not exist, redirect to noProduct page
				if (data.message === "Product does not exists") {
					navigate("/noProduct");
				} else {
					setName(data.product.name);
					setDescription(data.product.description);
					setPrice(data.product.price);
					setSource(data.product.source)
				}
				
			});

		// if user is not an admin, redirect to forbidden page
		} else if (user.isAdmin === false) {
			navigate("/forbidden");

		// if user is not authenticated, redirect to unauthorized page
		} else if (localStorage.getItem('token') == null) {
			navigate("/unauthorized");
		}
	// declare the dependencies
	},[user, productId, navigate]);

	// this useEffect checks if the name, description, price and source field is not empty and set isActive to true, otherwise set to false
	useEffect(() => {
		if (name !== '' && description !== '' && price !== '' && source !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	// declare the dependencies
	},[name, description, price, source]);

	// render the components
	return (
		<>
			{/* Product edit body section */}
			<Container className="my-5 py-3">				
				<Form onSubmit={e => editProduct(e)} className="p-4 p-lg-5 my-5 card-shadow">
					<div className="d-flex product-view-image mx-auto">
						<img src={source} alt="product_image" className="img-fluid" />
					</div>
					<Form.Group className="mb-3" controlId="productName">
						<Form.Label>Name</Form.Label>
						<Form.Control 
							type="text" 
							value={name} 
							onChange={e => setName(e.target.value)} 
						/>
					</Form.Group>
					<Form.Group className="mb-3" controlId="productDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control 
							as="textarea" 
							rows={3} 
							value={description}
							onChange={e => setDescription(e.target.value)}
						/>
					</Form.Group>
					<Form.Group className="mb-3" controlId="productPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control 
							type="number" 
							min={0}
							max={500000}
							step={100}
							value={price}
							onChange={e => setPrice(e.target.value)} 
						/>
					</Form.Group>
					<Form.Group className="mb-3" controlId="productSource">
						<Form.Label>Image source</Form.Label>
						<Form.Control 
							type="url" 
							value={source} 
							onChange={e => setSource(e.target.value)} 
						/>
					</Form.Group>
					<div className="d-flex justify-content-center justify-content-lg-end gap-2">
						{ (isActive) ? 
							<Button className="btn btn-info" type="submit">Save Changes</Button>
							:
							<Button className="btn btn-info" type="submit" disabled>Save Changes</Button>
						}
						<Button as={Link} to="/products" variant="danger">Cancel</Button>
					</div>
				</Form>
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>
	)
}

