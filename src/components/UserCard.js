// import built-in react modules
import { useContext } 			from 'react';
// import UserContext
import UserContext 				from '../UserContext';

// import component from react bootstrap
import { Button } 				from 'react-bootstrap';
// import Swal
import Swal 					from 'sweetalert2';

// import image from assets folder
import Profile 					from '../assets/profile.jpg';


// export the function so that it can be use anywhere
export default function AllUsers({userProp}) {

	// destructure the userProp
	const { firstName, lastName, email, mobileNo, isAdmin, _id } = userProp;

	// get user from UserContext
	const { user } = useContext(UserContext);

	// function to set a user as an admin
	function setAsAdmin(e) {
		fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/setAsAdmin`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successful
			if (data.message === "Successfully set user as admin") {
				Swal.fire({
	               title: "Successfully set user as admin!",
	               icon: "success"
	           	})

	        // shows an alert if operation failed
			} else {
				Swal.fire({
	               title: "Operation failed",
	               icon: "error",
	               text: "Cannot set user as admin!"
	            })
			};
		});
	}

	// function to set as a normal user
	function setAsNormalUser(e) {
		fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/setAsNormalUser`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successful
			if (data.message === "Successfully set as normal user") {
				Swal.fire({
	               title: "Successfully set as normal user",
	               icon: "success"
	           	})

	        // shows an alert if operation failed
			} else {
				Swal.fire({
	               title: "Operation failed",
	               icon: "error",
            	})
			}
		});
	}

	// render the components
	return (
		<>
			<div className="d-flex flex-column align-items-center justify-content-between bg-light card-shadow user-card gap-1 p-4 my-3 mx-auto">
				<img src={Profile} className="w-25 img-fluid" alt="profile_picture" />
				<h6>Name: {`${firstName} ${lastName}`}</h6>
				<h6>Email: {email}</h6>
				<h6>Mobile No: {mobileNo}</h6>
				<h6>Admin: {isAdmin.toString()}</h6>
				{ (isAdmin) ?
					(user.id !== _id) &&
					<Button className="align-self-center align-self-lg-end offwhite" onClick={e => setAsNormalUser(e)}>Set as normal user</Button>
					:
					<Button variant="primary" className="align-self-center align-self-lg-end" onClick={e => setAsAdmin(e)}>Set as admin</Button>
				}
			</div>
		</>
	)
}