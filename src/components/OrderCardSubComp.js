// import built-in react modules
import { useState, useEffect } 		from 'react';

// import components from react bootstrap
import { 
	Button,
	Form,
	OverlayTrigger,
	Tooltip 
} 									from 'react-bootstrap';
// import Swal
import Swal 						from 'sweetalert2';


// export the function so that it can be use anywhere
export default function OrderCardSubComp({dataProp, isCartProp}) {

	// destructure the dataProp
	const { productId, name, quantity, price, subtotal, _id } = dataProp;

	// set state for the updated quantity
	const [newQuantity, setNewQuantity] = useState(quantity);
	// set state for the image source
	const [source, setSource] = useState('');

	// set boolean state for conditional rendering
	const [isEditable, setIsEditable] = useState(false);

	// handle function to set isEditable to it's reverse current state
	const handleChange = (e) => {
		setIsEditable(!isEditable);
	}
 
 	// function for removing a single product from cart
	function removeProduct(e) {
		// use preventDefault to prevent the page from refreshing/reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/cart/removeProduct/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successfull
			if (data.message === "Product successfully removed from cart") {
				Swal.fire({
	            	title: "Product successfully removed from cart!",
	            	icon: "success"
	            })

	        // shows an alert if operation failed
			} else {
				Swal.fire({
	               title: "Operation failed",
	               icon: "error"	                
	            })
			}
		});
	}

	// function for updating product quantity on cart
	function updateQuantity(e) {
		// use preventDefault to prevent the page from refreshing/reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/cart/updateQuantity`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				orderId: _id,
				quantity: newQuantity 
			})
		})
		.then(res => res.json())
		.then(data => {
			// shows an alert if operation is successfull
			if (data.message === "Quantity successfully updated") {
				Swal.fire({
	               title: "Quantity successfully updated!",
	               icon: "success"
	           	})

	        // shows an alert if operation failed
			} else {
				Swal.fire({
	               title: "Operation failed",
	               icon: "error"	                
	            })
			}
		});

		// invoke handleChange
		handleChange();
		// set newQuantity to the updated quantity
		setNewQuantity(newQuantity);
	}

	// function to get the image source of the product
	function getSource() {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setSource(data.product.source);
		});
	}

	// pass the getSource to run it at first page render
	useEffect(() => {
		getSource();
	});
	// dependencies are optional

	// function for formatting the price, subtotal and totalAmount
	function formatPrice(price) {
		return price.toLocaleString(undefined, { style: 'currency', currency: 'PHP' })
	}

	// render the components
	return (
		<div className="d-flex flex-column flex-md-row primary-background text-white gap-4 p-3 mx-lg-2 my-lg-2 mx-0 my-1 w-100">
			<div className="bg-white rounded product-view-cart p-2 mx-auto">
				<img src={source} alt="product_image" className="img-fluid"/>
			</div>
			<div className="d-flex flex-column justify-content-evenly w-100">
				<h6>Name: {name}</h6>
				{!isCartProp && <h6>Quantity: {quantity}</h6>}
				<h6>Price: {formatPrice(price)}</h6>
				<h6>Subtotal: {formatPrice(subtotal)}</h6>
				{ isCartProp && 
					<>
						<div className="d-flex justify-content-start gap-2">
							<Form.Label><strong>Quantity</strong></Form.Label>
							{ (isEditable === false) ? 
								<Form.Control className="w-auto h-auto"
									type="number" 
									min={1}
									max={100}
									step={1}
									value={newQuantity}
									placeholder="Enter a number"
									disabled
								/>
								:
								<Form.Control className="w-auto h-auto"
									type="number" 
									min={1}
									max={100}
									step={1}
									value={newQuantity}
									placeholder="Enter a number"
									onChange={e => setNewQuantity(e.target.value)}
								/>
							}
							<OverlayTrigger placement={"top"} overlay={<Tooltip><strong>Edit</strong> quantity</Tooltip>}>
								<Button size="sm" className="btn btn-success" onClick={handleChange}><i className='bx bx-edit'></i></Button>
							</OverlayTrigger>
						</div>
						
						<div className="d-flex justify-content-center justify-content-lg-end mt-3">
							{ (isEditable === false) ? 
								<Button variant="info" size="sm" className="mr-2" disabled><i className='bx bx-save'></i></Button>
							:
								<OverlayTrigger placement={"top"} overlay={<Tooltip><strong>Save</strong> changes</Tooltip>}>
									<Button variant="info" size="sm" className="mr-2" onClick={e => updateQuantity(e)}><i className='bx bx-save'></i></Button>
								</OverlayTrigger>
							}
							<OverlayTrigger placement={"top"} overlay={<Tooltip><strong>Remove</strong> product</Tooltip>}>
								<Button variant="danger" size="sm" onClick={e => removeProduct(e)}><i className='bx bx-trash'></i></Button>
							</OverlayTrigger>
						</div>
					</>
				}
			</div>		
		</div>
	)
}