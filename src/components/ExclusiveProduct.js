// import built-in react modules
import { Link } 		from 'react-router-dom';

// import component from react bootstrap
import { Button } 	from 'react-bootstrap';


// export the function so that it can be use anywhere
export default function ExclusiveProduct() {

	// render the component
	return (
		<div className="offer">
	    	<div className="sm-container">
	    		<div className="row justify-content-center gap-5">
	    			<div className="col-2">
               	<img src={"https://i.ibb.co/jZtmCgJ/shimano-dura-ace-di2.png"} className="offer-img" alt="Shimano_dura_ace_di2"/>
               </div>
               <div className="col-2">
                  <p>Exclusively Available on BikeMeds</p>
                  <h1>Shimano Dura Ace Di2</h1>
                  <small>
                     The Dura-Ace Di2 R9250 Road groupset by Shimano consists of a 2x12-speed crank with 36-52 tooth gradation, a flat-mount disc brake set and an electronic rear derailleur and front derailleur for semi-wireless shifting. 
                  </small><br/>
                  <Button as={Link} to="/products/63904837d5002a4a038466c5" className="btn">Buy now &#10142;</Button>
                </div>
	    		</div>
	    	</div>
	   </div>
	)
}