// import component
import LandingPageText 		from './LandingPageText';

// import video from assets folder
import LandingVideo 		from '../assets/landing-page-clone.m4v';


// export the function so that it can be use anywhere
export default function VideoBackground() {
	return (
		<div className="min-vh-100">
			<div>
         		<video src={LandingVideo} className="video-background" type="video/mp4" autoPlay loop muted />
         	</div>
		   <div className="overlay"></div>
		   <LandingPageText />	    
      </div>
	)
}