// import built-in react modules
import { Link } 				from 'react-router-dom';

// import components from react bootstrap
import { Container, Button } 	from 'react-bootstrap';

// import components
import Footer 					from '../components/Footer';


// export the function so that it can be use anywhere
export default function Banner({bannerProp}) {

	// desctructure the bannerProp
	const { message, source, alt, submessage, label, route } = bannerProp;

	// render the component
	return(
		<>
			{/* Banner body section */}
			<Container className="text-center my-4 py-4">
				<div className="d-flex flex-column justify-content-center align-items-center mx-auto my-5 py-5 error-page-image-container">
					<h2>{message}</h2>
					<img src={source} alt={alt} className="img-fluid"/>
					<small className="m-2"><span>{submessage}</span></small>
					<Button as={Link} to={route} className="align-self-center primary-background m-1">{label}</Button>
				</div>
			</Container>

			{/* Fixed bottom footer */}
			<Footer/>
		</>
	)
}