// export the function so that it can be use anywhere
export default function Footer() {

	// render the component
	return(
		<div className="d-flex justify-content-center primary-background text-white fixed-bottom py-1">
			<p className="copyright">Copyright &copy; 2022 BikeMeds. All rights reserved.</p>
		</div>
	)
}