// import built-in react modules
import { 
    BrowserRouter as Router, 
    Routes, 
    Route 
}                               from 'react-router-dom';
import { useState, useEffect }  from 'react';
// import UserContext
import { UserProvider }         from './UserContext';

// import pages
import ProductDoesNotExist      from './pages/ProductDoesNotExist';
import UnauthorizedPage         from './pages/UnauthorizedPage';
import ForbiddenPage            from './pages/ForbiddenPage';
import Landing                  from './pages/Landing';
import Login                    from './pages/Login';
import Logout                   from './pages/Logout';
import Products                 from './pages/Products';
import Register                 from './pages/Register';
import Profile                  from './pages/Profile';
import Cart                     from './pages/Cart';
import Orders                   from './pages/Orders';
import Users                    from './pages/Users';
import AllOrders                from './pages/AllOrders';
import EmptyCart                from './pages/EmptyCart';
import EmptyOrder               from './pages/EmptyOrder';
import NoOrders                 from './pages/NoOrders';
import ErrorPage                from './pages/ErrorPage';

// import components
import AppNavbar                from './components/AppNavbar';
import ProductView              from './components/ProductView';
import ProductEditView          from './components/ProductEditView';

// import css
import './App.css';


function App() {
    // this userContext will store the id and isAdmin property of the user
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    // this userContext will clear everything on the localStorage
    const unsetUser = () => {
        localStorage.clear();
    };

    /*
        1. orderDetails - will contain the details such as productId and quantity during multiple add to cart and create order
        2. addToCartState, buyNowState - is a boolean state which will trigger conditional rendering on the check queue feature 
    */
    const [orderDetails, setOrderDetails] = useState([]);
    const [addToCartState, setAddToCartState] = useState(false);
    const [buyNowState, setBuyNowState] = useState(false);

    // this will get the details of the user and assign the id and isAdmin property to the user state
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/getUserDetails`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.auth === "Error 401 - Unauthorized") {
                setUser({
                    id: null,
                    isAdmin: null
                });
            } else if (typeof data.details._id !== "undefined") {
                setUser({
                    id: data.details._id,
                    isAdmin: data.details.isAdmin
                });
            } 
        });
    },[]);

    // render the components and set the routes
    return (
        // this provides all contents of the userContext to the components listed for every route
        <UserProvider value={{user, setUser, unsetUser, orderDetails, setOrderDetails, addToCartState, setAddToCartState, buyNowState, setBuyNowState}}>
            <Router>
            <AppNavbar/>
               <Routes>
                  <Route path="/products/:productId" element={<ProductView/>}/>
                  <Route path="/products/edit/:productId" element={<ProductEditView/>}/>
                  <Route path="/products" element={<Products/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/profile" element={<Profile/>}/>
                  <Route path="/cart" element={<Cart/>}/>
                  <Route path="/orders" element={<Orders/>}/>
                  <Route path="/admin/users" element={<Users/>}/>
                  <Route path="/admin/orders" element={<AllOrders/>}/>
                  <Route path="/emptyCart" element={<EmptyCart/>}/>
                  <Route path="/emptyOrder" element={<EmptyOrder/>}/>
                  <Route path="/noOrders" element={<NoOrders/>}/>
                  <Route path="/noProduct" element={<ProductDoesNotExist/>}/>
                  <Route path="/forbidden" element={<ForbiddenPage/>}/>
                  <Route path="/unauthorized" element={<UnauthorizedPage/>}/>
                  <Route path="/" element={<Landing/>}/>
                  <Route path="*" element={<ErrorPage/>}/>
               </Routes>
            </Router>
        </UserProvider>
    );
}

// export the function so that it can be used anywhere
export default App;
